# Kubernetes

### Project structure
- Manifests: Contains all the kubernetes yaml files
- Helm: Contains all the helm charts
### Use manifests
After fill TODO section in each yaml file, run below command to apply
```
kubectl apply -f manifest.yaml
```
### Use helm
- Run a below command to store registry credential to kubernetes for private registries
```
kubectl create secret docker-registry <NAME> --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email>
```
- Make a copy of values.yaml and change it based on your requirements.
- Run below command to install helm chart
```
helm install <RELEASE_NAME> <CHART_NAME> -f values.yaml
```
Note: <CHART_NAME> can be a path of helm chart folder or a name of helm chart if it is already added to helm repo.

### Notes
- Ingress: I did not use ingress because of this use case does not need it basically. But if you need layer 7 routing, or you want to use a valid IP address for multiple service you can use ingress and route traffic based on hostname or path.
- Vault: One of the best practices is using vault for storing secrets. Hashicorp vault can be used for storing secrets and injecting them to pods as environment variables or files. It is also possible to use vault for dynamic secrets.
- Application Logging: If you need logs you can use fluent-beat or any other beats as a side-car to collect and send logs.
- Application Monitoring: Based on requirements solutions can be different but one of the best solutions is use Cilium as CNI and use hubble for status code response and similar monitoring 