#!/bin/sh

gunicorn main:app --bind ${FLASK_HOST:-0.0.0.0}:${FLASK_PORT:-8000} --workers ${GUNICORN_WORKER:-2} --threads ${GUNICORN_THREADS:-4} --timeout ${GUNICORN_TIMEOUT:-2} --access-logfile - --log-level ${GUNICORN_LOG_LVL:-info}
