import time
import random

from decouple import config
from flask import Flask
from redis import Redis, exceptions

# VARIABLES
REDIS_HOST = config('REDIS_HOST', cast=str, default='redis')
REDIS_PORT = config('REDIS_PORT', cast=int, default=6379)
REDIS_DB = config('REDIS_DB', cast=int, default=0)
REDIS_PASSWORD = config('REDIS_PASSWORD', cast=str, default='password')
FLASK_DEBUG = config('FLASK_DEBUG', cast=bool, default=False)
FLASK_HOST = config('FLASK_HOST', cast=str, default='0.0.0.0')
FLASK_PORT = config('FLASK_PORT', cast=int, default=8000)

app = Flask(__name__)
r = Redis(
    host=REDIS_HOST,
    port=REDIS_PORT,
    db=REDIS_DB,
    password=REDIS_PASSWORD,
)


@app.route('/ping', methods=['GET'])
def ping():
    try:
        time.sleep(random.uniform(0.01, 0.1))
        pong = r.ping()
        if pong:
            return '', 200
        return '', 500
    except exceptions.ConnectionError as e:
        app.logger.error(e)
        return '', 500


@app.route('/health', methods=['GET'])
def health():
    return '', 200


if __name__ == '__main__':
    app.run(
        debug=FLASK_DEBUG,
        use_reloader=FLASK_DEBUG,
        host=FLASK_HOST,
        port=FLASK_PORT,
    )
