import unittest
from unittest.mock import patch

from redis import exceptions

from main import app, r


class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    @patch.object(r, 'ping', return_value=True)
    def test_successful_ping(self, mock_ping):
        response = self.app.get('/ping')
        self.assertEqual(response.status_code, 200)
        mock_ping.assert_called_once()

    @patch.object(r, 'ping', return_value=False)
    def test_unsuccessful_ping(self, mock_ping):
        response = self.app.get('/ping')
        self.assertEqual(response.status_code, 500)
        mock_ping.assert_called_once()

    @patch.object(r, 'ping', side_effect=exceptions.ConnectionError)
    def test_connection_error_during_ping(self, mock_ping):
        response = self.app.get('/ping')
        self.assertEqual(response.status_code, 500)
        mock_ping.assert_called_once()

    def test_successful_health_check(self):
        response = self.app.get('/health')
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
