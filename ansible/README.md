# Ansible
Deploy application on VM or bare metal server using Ansible.

### Usage
- Setup HAProxy as load balancer with suggested `haproxy.cfg` file in the root of project.
- Set the environment variable in gitlab ci variable as file type with name of `monitoring_env`
- Create `inventory`
- run the ansible playbook with below variables:
```
REGISTRY_URL
REGISTRY_USERNAME
REGISTRY_PASSWORD
IMAGE_NAME
```
for example:
```
ansible-playbook -i inventory deploy.yml --extra-vars "REGISTRY_URL=registry.gitlab.com REGISTRY_USERNAME=gitlab-username REGISTRY_PASSWORD=gitlab-password IMAGE_NAME=registry.gitlab.com/username/project:tag"
```