# README
### In this README I explain some of the decisions I made while developing the project and best practices that I can and can't follow due to the time constraint and limited resources.

#### Project structure

The project is divided into three main folders: `app`, `kubernetes` and `ansible`.

- App folder contains the source code of the Application, Dockerfiles and [simple documentation](app/README.md) on how to run and develop the application.

- Kubernetes folder contains the kubernetes manifest and helm chart for the application, also a [README](kubernetes/README.md) to explain some extra information.

- Ansible folder contains the ansible playbook to deploy the application in a bare-metal server, It also contains a [README](ansible/README.md).

#### Zero down time

There are multiple solution to achieve zero down time, such as `blue-green deployment`, `canary deployment`, `rolling update` and `A/B testing`.

A simple solution for this project: configure HAProxy as LB with activated health check and two server as backend, 
then deploy an instance of application that points to HAProxy's server1 configuration so when you want release new version of application, 
you can deploy the new version of application and point it to HAProxy's server2 configuration, then you can remove the old version of application.
you can continue this process for each new version of application.


#### Gitlab ci
- To connect to and use kubernetes cluster in gitlab ci there are multiple ways like gitlab kubernetes agent but in this project I used kubernetes configuration in gitlab ci variables as file to be more flexible and usable in different clusters as soon as possible.
- Before run the pipeline you should create a variable in gitlab ci with name `KUBE_CONFIG` and value of your kubernetes configuration file content.
- Set the `KUBE_NAMESPACE` variable to the namespace that you want to deploy the application.
- Set the `HELM_VALUES` file variable [More info](kubernetes/README.md#use-helm).